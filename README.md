# Hoja05_LDAP_Repaso

1. Crear en LDAP una nueva organizationalUnit llamada alumnos. Crea también 6 usuarios
pertenecientes a esa unidad. Por último, crea dos grupos (groupOfUniqueNames) llamados
daw1 y daw2. De los 6 usuarios creados antes, habrá 3 en cada grupo.
Para ello harás un fichero con extensión ldif que deberás añadir al LDAP.

![Captura1](img/captura1.jpg "Captura 1")
![Captura2](img/captura2.jpg "Captura 2")

2. Crea a continuación un proyecto dinámico en Eclipse (Dynamic Web Project) llamado
repaso. En él haz un fichero index.jsp que muestre la hora actual.
	
	* ![Captura3](img/captura3.jpg "Captura 3")
	* ![Captura4](img/captura4.jpg "Captura 4") 
	* ![Captura5](img/captura5.jpg "Captura 5")
	
3. A continuación, configuraremos la aplicación repaso utilizando JNDIRealm para que sólo
puedan acceder los alumnos del grupo daw2.

 * Para hacer la parte 5, hay que crear primero un fichero context.xml, para ello clickamos derecho encima de meta inf, le damos a new, other, y buscamos el xml file
 * ![Captura6](img/captura6.jpg "Captura 6")
 * ![Captura7](img/captura7.jpg "Captura 7")

	
4. Para importar los usuarios de mi ldif.
	* ![Captura8](img/captura8.jpg "Captura 8")

5. Para el punto 5 hay que crear un archivo login.jsp como el de tienda en web inf.

* ![Captura9](img/captura9.jpg "Captura 9")

6. Para el apartado 6 copio el archivo build.xml del ejercicio de la tienda y le cambio algunas cosas.

* ![Captura10](img/captura10.jpg "Captura 10")
* ![Captura11](img/captura11.jpg "Captura 11")
* Y ya podemos crear el war y hacer el deploy
* ![Captura12](img/captura12.jpg "Captura 12")
* ![Captura13](img/captura13.jpg "Captura 13")

7. Para arreglar el error de los jsp, tuve que añadir la librería de tomcat.

* Click derecho en el proyecto, properties:
* ![Captura14](img/captura14.jpg "Captura 14")
* ![Captura15](img/captura15.jpg "Captura 15")

8. Para crear el host virtual repaso.com

* sudo nano /etc/hosts
* ![Captura16](img/captura16.jpg "Captura 16")
* sudo mkdir /var/lib/tomcat9/webapps-repaso
* Le doy permisos:
* ![Captura17](img/captura17.jpg "Captura 17")
* Edita el fichero /var/lib/tomcat9/conf/server.xml y habilita el Host virtual:

* ![Captura18](img/captura18.jpg "Captura 18")
* Reinicio tomcat.
* ![Captura19](img/captura19.jpg "Captura 19")
* ![Captura20](img/captura20.jpg "Captura 20")
* ![Captura21](img/captura21.jpg "Captura 21")

9. Haz que la aplicación sólo funcione por SSL

* ![Captura22](img/captura22.jpg "Captura 22")

* Hago undeploy y la vuelvo a desplegar.
* ![Captura23](img/captura23.jpg "Captura 23")
* ![Captura24](img/captura24.jpg "Captura 24")





